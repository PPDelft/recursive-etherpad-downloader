# Recursive Etherpad downloader

Export all your pads for archiving or for transfer to another etherpad server.

Is your favorite etherpad server going offline soon, and you have a lot of work on it? (what is not really the intended use for etherpad, but hey, finalizing, publishing and storing your work is always manjana)

REQUIREMENT: you should have an INDEX_PAD; either an online etherpad or a local .txt file.

This script is recursive, so it searches the downloaded pads for links to not yet known pads on the same server and downloads these too.

Of course you can also use this script to create a backup before the end of your favorite server is announced ;-)

Run the script in the directory where you want to store your downloaded pads.

To overcome DOS mitigation measures on the pad server a sleep timeout variable (SPEED) is used. Feel free to adapt.

Overview of etherpad export file types:
.txt  (downloaded by default)
.etherpad (downloaded by default)
.html (downloaded by default)
.pdf
.odt
.mediawiki

No bash on your system because its Windows? The proper solution for that is to upgrade to linux, the second best is https://www.wikihow.com/Enable-the-Windows-Subsystem-for-Linux Do pay attention to *NIX vs Windows line-endings
