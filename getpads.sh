#!/bin/bash		   
#### RECURSIVE ETHERPAD DOWNLOADER - This Bash Shell script exports all your pads so you can import them on an other server. ####
# Is your favorite etherpad server going offline soon, and you have a lot of work on it? (what is not really the intended use for etherpad, but hey, finalizing, publishing and storing your work is always manjana) 
# REQUIREMENT: you should have an INDEX_PAD; either an online etherpad or a local .txt file.
# This script is recursive, so it searches the downloaded pads for links to not yet known pads on the same server and downloads these too.
# Of course you can also use this script to create a backup before the end of your favorite server is announced ;-)
# Run the script in the directory where you want to store your downloaded pads.
# To overcome DOS mitigation measures on the pad server a sleep timeout variable (SPEED) is used. Feel free to adapt.
# Overview of etherpad export file types:
#   .txt  (downloaded by default)
#   .etherpad (downloaded by default)
#   .html (downloaded by default)
#   .pdf 
#   .odt 
#   .mediawiki 
# No bash on your system because its Windows? The proper solution for that is to upgrade to linux, the second best is https://www.wikihow.com/Enable-the-Windows-Subsystem-for-Linux Do pay attention to *NIX vs Windows line-endings

# CONFIGURATION START
BASE_URL="https://etherpad.pp-international.net/p/" # Base URL for all pads.
INDEX_PAD="testjedebestje" # Name of your index pad containing links to other pads, must exist on base URL, OR as text file in current directory named $INDEX_PAD.txt.
SPEED=5 # Pause before fetching (next) file. Adjust if you want to go faster, or slower.
# CONFIGURATION END

echo $BASE_URL$INDEX_PAD > newpads.lst
echo "Starting script using index pad @ $BASE_URL$INDEX_PAD"
no_of_urls=$(wc -l < newpads.lst) # no_of_urls=$(sed -n '$=' newpads.lst)

while [ $no_of_urls -gt 0 ] # Continue until no new pad urls are found
do
	# Check if the initial index pad is already there
	if ! [[ -s $INDEX_PAD.txt ]]; then
		echo "Here we go!..."
	else
		# These pads are already successfully retrieved
		find . -maxdepth 1 -type f -regextype posix-egrep -size +0 -regex ".*\.(txt|etherpad)$" | sed 's/\.[^.]*$//' | sed 's#./##g' | uniq -di | awk -v b="$BASE_URL" '{print b$1}' | sort -uf > retrievedpads.lst # for entry in *.txt; do echo "$BASE_URL${entry%%.*}"; done | sort -u > retrievedpads.lst
		# Search for pad URLs in all .txt and put them in the foundpads list
		cat *.txt retrievedpads.lst | grep -Eo "$BASE_URL[a-zA-Z0-9.?=_%-]*" | awk '!/\/export/ && !/\/timeslider/' | sort -uf > foundpads.lst
		# Check for etherpad links that are not retrieved yet
		if diff -q foundpads.lst retrievedpads.lst --ignore-case; then
			echo "Filelists are identical, no new pads found"
			no_of_urls=0
			rm newpads.lst
			touch newpads.lst
			break
		else
			# Create new file with new (not yet retrieved) pad urls
			if diff foundpads.lst retrievedpads.lst --suppress-common-lines --ignore-case | grep "^<" | sed 's#< ##g' | sort -u > newpads.lst; then
				no_of_urls=$(wc -l < newpads.lst)
			else
				echo "No new pads found"
				no_of_urls=0
				rm newpads.lst
				touch newpads.lst
				break
			fi
		fi
	fi
	echo ----------------------------------------------------------------------
	echo --------- TRYING TO RETRIEVE PADS FROM $no_of_urls NEW URLS ----------
	echo ----------------------------------------------------------------------
	i=0
	while read URLLINE
	do
		let i++
		# Fetch filename form URL
		PAD=${URLLINE/${BASE_URL////\\/}/};
		# Show what pad is checked for presence or downloaded
		echo $i/$no_of_urls $PAD
		### Part to download .etherpad ###
		# Fetch only if file does not exist yet
		if ! [[ -s $PAD.etherpad ]]; then
			# Sleep to make sure script doesn't look like a DOS attack on the pad server. Adjust if you want to go faster, or slower.
			sleep $SPEED
			# Fetch pad with wget and correct filename
			(echo "wget -4 -O $PAD.etherpad $URLLINE/export/etherpad";) | bash
		fi
		### End part to download .etherpad ###
		### Part to download .txt ###
		# Fetch only if file does not exist yet
		if ! [[ -s $PAD.txt ]]; then
			# Sleep to make sure script doesn't look like a DOS attack on the pad server. Adjust if you want to go faster, or slower.
			sleep $SPEED
			# Fetch pad with wget and correct filename
			(echo "wget -4 -O $PAD.txt $URLLINE/export/txt";) | bash
		fi
		### End part to download .txt ###
		### Part to download .html ###
		# Fetch only if file does not exist yet
		if ! [[ -s $PAD.html ]]; then
			# Sleep to make sure script doesn't look like a DOS attack on the pad server. Adjust if you want to go faster, or slower.
			sleep $SPEED
			# Fetch pad with wget and correct filename
			(echo "wget -4 -O $PAD.html $URLLINE/export/html";) | bash
		fi
		### End part to download .html ###
		### Part to download your additional filetype ###
		### End part to download your additional filetype ###
	done < newpads.lst
done

# Possible improvements:
# - Script is checking same files over and over again, this could be made more efficient
# - Create an array of filetypes to download instead of copying are piece of code
# - Accept a list of etherpad-servers instead of a single server. (is this usefull for someone?) 
# - Script is now only checking 'is the pad downloaded or not', maybe it would be nice to check if there were modifications on the time slider after the file save date. (Or just start in an empty dir, then if will fetch all new versions) Also, a bash script does not seem like the richt tool for that.